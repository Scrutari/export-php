<?php
/***************************************************************
* SDE - PHP implementation of ScrutariDataExport API
* http://www.scrutari.net/dokuwiki/scrutaridata:exportapi
*
* Copyright (c) 2010-2016 Vincent Calame - Exemole
* Licensed under MIT (http://en.wikipedia.org/wiki/MIT_License)
*
*
* Ce fichier réunit toutes les classes implémentant l'API ScrutariDataExport ainsi qu'une classe utilitaire.
*
* Classes et constantes sont préfixées par SDE_ pour éviter tout conflit.
*
* Le départ de l'exportation se fait par la création d'une instance de SDE_XmlWriter suivi par celle
* d'une instance de SDE_ScrutariDataExport
* 
* Exemple :
* $file = fopen("scrutari-data.xml", "w");
* $xmlWriter = new SDE_XmlWriter($file, true, true);
* $scrutariDataExport = new SDE_ScrutariDataExport($xmlWriter);
*************************************************************/

define("SDE_START", 1);
define("SDE_BASEMETADATA", 2);
define("SDE_CORPUSMETADATA", 3);
define("SDE_FICHE", 4);
define("SDE_THESAURUSMETADATA", 5);
define("SDE_MOTCLE", 6);
define("SDE_END", 7);

define("SDE_INTITULE_SHORT", 1);
define("SDE_INTITULE_LONG", 2);
define("SDE_INTITULE_CORPUS", 1);
define("SDE_INTITULE_FICHE", 2);
define("SDE_INTITULE_THESAURUS", 1);

/**
* Voir http://www.scrutari.net/dokuwiki/scrutaridata:exportapi:scrutaridataexport
* $xmlWriter est une instance de l'utilitaire xmlWriter
*/
class SDE_ScrutariDataExport {
  private $currentState = SDE_START;
  private $xmlBuilder = NULL;
  private $xmlWriter;
  private $ficheExport;
  private $motcleExport;
  private $indexationMap = array();
        
  public function __construct($xmlWriter) {
    $this->xmlWriter = $xmlWriter;
    $this->ficheExport =  new SDE_FicheExport();
    $this->motcleExport = new SDE_MotcleExport();
  }

  public function startExport() {
    if ($this->currentState != SDE_START) {
      throw new SDE_ExportStateException('startExport / '.$this->currentState);
    }
    $this->xmlWriter->openTag("base");
    $this->currentState = SDE_BASEMETADATA;
    $baseMetadataExport = new SDE_BaseMetadataExport();
    $this->xmlBuilder = $baseMetadataExport;
    return $baseMetadataExport;
  }

  public function newCorpus($corpusName) {
    $this->commonTest('newCorpus');
    $this->flushXmlBuilder();
    $this->flushSubset();
    $this->xmlWriter->openTagWithAttribute("corpus", "corpus-name", $corpusName);
    $this->currentState = SDE_CORPUSMETADATA;
    $corpusMetadataExport = new SDE_CorpusMetadataExport();
    $this->xmlBuilder = $corpusMetadataExport;
    return $corpusMetadataExport;
  }

  public function newFiche($ficheId) {
    switch($this->currentState) {
      case SDE_START :
      case SDE_END :
      case SDE_BASEMETADATA :
      case SDE_THESAURUSMETADATA :
      case SDE_MOTCLE :
        throw new SDE_ExportStateException('newFiche'.' / '.$this->currentState);
    }
    $this->flushXmlBuilder();
    $this->currentState = SDE_FICHE;
    $this->ficheExport->reinit($ficheId);
    $this->xmlBuilder = $this->ficheExport;
    return $this->ficheExport;
  }

  public function newThesaurus($thesaurusName) {
    $this->commonTest('newThesaurus');
    $this->flushXmlBuilder();
    $this->flushSubset();
    $this->xmlWriter->openTagWithAttribute("thesaurus", "thesaurus-name", $thesaurusName);
    $this->currentState = SDE_THESAURUSMETADATA;
    $thesaurusMetadataExport = new SDE_ThesaurusMetadataExport();
    $this->xmlBuilder = $thesaurusMetadataExport;
    return $thesaurusMetadataExport;
  }

  public function newMotcle($motcleId) {
    switch($this->currentState) {
      case SDE_START :
      case SDE_END :
      case SDE_BASEMETADATA :
      case SDE_CORPUSMETADATA :
      case SDE_FICHE :
        throw new SDE_ExportStateException('newMotcle'.' / '.$this->currentState);
    }
    $this->flushXmlBuilder();
    $this->currentState = SDE_MOTCLE;
    $this->motcleExport->reinit($motcleId);
    $this->xmlBuilder = $this->motcleExport;
    return $this->motcleExport;
  }

  public function addIndexation($corpusName, $ficheId, $thesaurusName, $motcleId, $poids) {
    $this->commonTest('addIndexation');
    $subsetKey = $corpusName."\t".$thesaurusName;
    $itemKey = $ficheId."\t".$motcleId;
    if (array_key_exists($subsetKey, $this->indexationMap)) {
      $this->indexationMap[$subsetKey][$itemKey] = $poids;
    }
    else {
      $this->indexationMap[$subsetKey] = array($itemKey => $poids);
    }
  }

  public function endExport() {
    $this->commonTest('endExport');
    $this->flushXmlBuilder();
    $this->flushSubset();
    $this->writeIndexation();
    $this->xmlWriter->closeTag("base");
    $this->currentState = SDE_END;
  }

  public function getState() {
    return $this->currentState;
  }

  private function commonTest($methodName) {
    switch($this->currentState) {
      case SDE_START :
      case SDE_END :
        throw new SDE_ExportStateException($methodName.' / '.$this->currentState);
    }
  }
  
  private function flushXmlBuilder() {
        if (!is_Null($this->xmlBuilder)) {
            $this->xmlBuilder->writeXML($this->xmlWriter);
            $this->xmlBuilder = NULL;
        }
    }

  private function flushSubset() {
      switch($this->currentState) {
          case SDE_CORPUSMETADATA :
          case SDE_FICHE :
              $this->xmlWriter->closeTag("corpus");
              break;
          case SDE_THESAURUSMETADATA :
          case SDE_MOTCLE :
              $this->xmlWriter->closeTag("thesaurus");
              break;
      }
  }
  
  private function writeIndexation() {
    $indexationGroupMap = array("corpus-path" =>"","thesaurus-path" => "");
    $indexationMap = array("fiche-id" =>"","motcle-id" => "", "poids" => 1);
    foreach ($this->indexationMap as $subsetKey => $idxMap) {
      $pos1 = strpos($subsetKey, "\t");
      $indexationGroupMap["corpus-path"] = substr($subsetKey, 0, $pos1);
      $indexationGroupMap["thesaurus-path"] = substr($subsetKey, $pos1 + 1);
      $this->xmlWriter->openTagWithAttributes("indexation-group",$indexationGroupMap);
      foreach($idxMap as $itemKey => $poids) {
        $pos2 = strpos($itemKey, "\t");
        $indexationMap["fiche-id"] = substr($itemKey, 0, $pos2);
        $indexationMap["motcle-id"] = substr($itemKey, $pos2 + 1);
        if ($poids > 1) {
          $indexationMap["poids"] = $poids;
        } else {
          if (array_key_exists("poids", $indexationMap)) unset($indexationMap["poids"]);
        }
        $this->xmlWriter->addEmptyElement("indexation", $indexationMap);
      }
      $this->xmlWriter->closeTag("indexation-group");
    }
  }

}

/**
* Implémentation de http://www.scrutari.net/dokuwiki/scrutaridata:exportapi:basemetadataexport
*/
class SDE_BaseMetadataExport {
  private $authority = "";
  private $baseName = "";
  private $baseIcon = "";
  private $langUIArray = array();
  private $shortMap = array();
  private $longMap = array();

  public function setAuthority($authority) {
    $this->authority = $authority;
  }
    
  public function setBaseName($baseName) {
    $this->baseName = $baseName;
  }
  
  public function setBaseIcon($baseIcon) {
    $this->baseIcon = $baseIcon;
  }
    
  public function setIntitule($intituleType, $lang, $intituleValue) {
    switch($intituleType) {
      case SDE_INTITULE_SHORT:
        $this->shortMap[$lang] = $intituleValue;
        break;
      case SDE_INTITULE_LONG:
        $this->longMap[$lang] = $intituleValue;
        break;
      default :
        throw new Exception("Wrong intituleType = " + $intituleType);
    }
  }
  
  public function addLangUI($lang){
    $this->langUIArray[] = $lang;
  }
  
  public function writeXML(SDE_XmlWriter $xmlWriter) {
    $xmlWriter->openTag("base-metadata");
    $xmlWriter->addSimpleElement("authority", $this->authority);
    $xmlWriter->addSimpleElement("base-name", $this->baseName);
    $xmlWriter->addSimpleElement("base-icon", $this->baseIcon);
    $this->addMap(SDE_INTITULE_SHORT, $this->shortMap, $xmlWriter);
    $this->addMap(SDE_INTITULE_LONG, $this->longMap, $xmlWriter);
    $size = count($this->langUIArray);
    if ($size > 0) {
      $xmlWriter->openTag("langs-ui");
      for ($i = 0; $i < $size; $i++) {
        $xmlWriter->addSimpleElement("lang", $this->langUIArray[$i]);
      }
      $xmlWriter->closeTag("langs-ui");
    }
    $xmlWriter->closeTag("base-metadata");
  }

  private function addMap($intituleType, $map, SDE_XmlWriter $xmlWriter) {
    if (count($map) == 0) {
      return;
    }
    $suffix = $this->getSuffix($intituleType);
    $xmlWriter->openTag("intitule-".$suffix);
    foreach($map as $cle => $valeur) {
      $xmlWriter->addLibElement($cle, $valeur);
    }
    $xmlWriter->closeTag("intitule-".$suffix);
  }
  
  private function getSuffix($intituleType) {
    switch ($intituleType) {
      case SDE_INTITULE_SHORT:
        return "short";
      case SDE_INTITULE_LONG:
        return "long";
      default:
        throw new Exception("Wrong intituleType = " + $intituleType);
    }
  }
  
}

/**
* Implémentation de http://www.scrutari.net/dokuwiki/scrutaridata:exportapi:corpusmetadataexport
*/
class SDE_CorpusMetadataExport {
  private $corpusIcon = "";
  private $hrefParent = "";
  private $corpusMap = array();
  private $ficheMap = array();
  private $complementMapArray = array();

  public function setCorpusIcon($corpusIcon) {
    $this->corpusIcon = $corpusIcon;
  }

  public function setHrefParent($hrefParent) {
    $this->hrefParent = $hrefParent;
  }
  
  public function setIntitule($intituleType, $lang, $intituleValue) {
    switch($intituleType) {
      case SDE_INTITULE_CORPUS:
        $this->corpusMap[$lang] = $intituleValue;
        break;
      case SDE_INTITULE_FICHE:
        $this->ficheMap[$lang] = $intituleValue;
        break;
      default :
        throw new Exception("Wrong intituleType = " + $intituleType);
    }
  }

  public function addComplement() {
    $this->complementMapArray[] = array();
    return count($this->complementMapArray);
  }

  public function setComplementIntitule($complementNumber, $lang, $intituleValue) {
    if (($complementNumber < 0) || ($complementNumber > count($this->complementMapArray))) {
        return;
    }
    $this->complementMapArray[$complementNumber -1][$lang] = $intituleValue;
  }

  public function writeXML(SDE_XmlWriter $xmlWriter) {
    $xmlWriter->openTag("corpus-metadata");
    $this->addMap(SDE_INTITULE_CORPUS, $this->corpusMap, $xmlWriter);
    $this->addMap(SDE_INTITULE_FICHE, $this->ficheMap, $xmlWriter);
    $xmlWriter->addSimpleElement("href-parent", $this->hrefParent);
    $xmlWriter->addSimpleElement("corpus-icon", $this->corpusIcon);
    for($i = 0; $i < count($this->complementMapArray); $i++) {
      $xmlWriter->openTag("complement-metadata");
      foreach($this->complementMapArray[$i] as $cle => $valeur) {
        $xmlWriter->addLibElement($cle, $valeur);
      }
      $xmlWriter->closeTag("complement-metadata");
    }
    $xmlWriter->closeTag("corpus-metadata");
  }
  
  private function addComplementMap($map, SDE_XmlWriter $xmlWriter) {
    $xmlWriter->openTag("complement-metadata");
    foreach($map as $cle => $valeur) {
      $xmlWriter->addLibElement($cle, $valeur);
    }
    $xmlWriter->closeTag("complement-metadata");
  }
  
  private function addMap($intituleType, $map, SDE_XmlWriter $xmlWriter) {
    if (count($map) == 0) {
      return;
    }
    $suffix = $this->getSuffix($intituleType);
    $xmlWriter->openTag("intitule-".$suffix);
    foreach($map as $cle => $valeur) {
      $xmlWriter->addLibElement($cle, $valeur);
    }
    $xmlWriter->closeTag("intitule-".$suffix);
  }
  
  private function getSuffix($intituleType) {
    switch ($intituleType) {
      case SDE_INTITULE_CORPUS:
        return "corpus";
      case SDE_INTITULE_FICHE:
        return "fiche";
      default:
        throw new Exception("Wrong intituleType = " + $intituleType);
    }
  }
  
}

/**
* Implémentation de http://www.scrutari.net/dokuwiki/scrutaridata:exportapi:ficheexport
*/
class SDE_FicheExport {
  private $ficheId = "";
  private $titre = "";
  private $soustitre = "";
  private $date = "";
  private $lang = "";
  private $href = "";
  private $ficheIcon = "";
  private $complementArray = array();
  private $attributeArray = array();

  public function reinit($ficheId) {
    $this->ficheId = $ficheId;
    $this->titre = "";
    $this->soustitre = "";
    $this->date = "";
    $this->lang = "";
    $this->href = "";
    $this->ficheIcon = "";
    $this->complementArray = array();
    $this->attributeArray = array();
  }

  public function setTitre($titre) {
    $this->titre = $titre;
  }

  public function setSoustitre($soustitre) {
    $this->soustitre = $soustitre;
  }

  public function setDate($date) {
    $this->date = $date;
  }

  public function setLang($lang) {
    $this->lang = $lang;
  }

  public function setHref($href) {
    $this->href = $href;
  }

  public function setFicheIcon($ficheIcon) {
    $this->ficheIcon = $ficheIcon;
  }

  public function addComplement($complementNumber, $complementValue) {
    if ($complementNumber < 1) return;
    $this->complementArray[$complementNumber] = $complementValue;
  }
  
  public function addAttributeValue($nameSpace, $localKey, $attributeValue) {
    $key = $nameSpace.":".$localKey;
    if (array_key_exists($key, $this->attributeArray)) {
      $this->attributeArray[$key][] = $attributeValue;
    } else {
      $this->attributeArray[$key] = array($attributeValue);
    }
  }

  public function writeXML(SDE_XmlWriter $xmlWriter) {
    $xmlWriter->openTagWithAttribute("fiche","fiche-id", $this->ficheId);
    $xmlWriter->addSimpleElement("titre", $this->titre);
    $xmlWriter->addSimpleElement("soustitre", $this->soustitre);
    $xmlWriter->addSimpleElement("date", $this->date);
    $xmlWriter->addSimpleElement("lang", $this->lang);
    $xmlWriter->addSimpleElement("href", $this->href);
    $xmlWriter->addSimpleElement("fiche-icon", $this->ficheIcon);
    if (count($this->complementArray) > 0) {
      $max_key = max(array_keys($this->complementArray));
      for ($i = 1; $i <= $max_key; $i++) {
        $value = "";
        if (array_key_exists($i,$this->complementArray)) $value =  $this->complementArray[$i];
        if (strlen($value) > 0) $xmlWriter->addSimpleElement("complement", $value);
        else $xmlWriter->addEmptyElement("complement", NULL);
      }
    }
    foreach($this->attributeArray as $key => $values) {
      $keyArray = explode(':', $key);
      $keyMap = array("ns" => $keyArray[0], "key" => $keyArray[1]);
      $xmlWriter->openTagWithAttributes("attr", $keyMap);
      $valCount = count($values);
      for($i = 0; $i < $valCount; $i++) {
        $xmlWriter->addSimpleElement("val", $values[$i]);
      }
      $xmlWriter->closeTag("attr");
    }
    $xmlWriter->closeTag("fiche");
  }
  
}

/**
* Implémentation de http://www.scrutari.net/dokuwiki/scrutaridata:exportapi:thesaurusmetadataexport
*/
class SDE_ThesaurusMetadataExport {
  private $thesaurusMap = array();
  
  public function setIntitule($intituleType, $lang, $intituleValue) {
    switch($intituleType) {
      case SDE_INTITULE_THESAURUS:
        $this->thesaurusMap[$lang] = $intituleValue;
        break;
      default :
        throw new Exception("Wrong intituleType = " + $intituleType);
    }
  }

  public function writeXML(SDE_XmlWriter $xmlWriter) {
    $xmlWriter->openTag("thesaurus-metadata");
    $this->addMap(SDE_INTITULE_THESAURUS, $this->thesaurusMap, $xmlWriter);
    $xmlWriter->closeTag("thesaurus-metadata");
  }
  
  private function addMap($intituleType, $map, SDE_XmlWriter $xmlWriter) {
    if (count($map) == 0) {
      return;
    }
    $suffix = $this->getSuffix($intituleType);
    $xmlWriter->openTag("intitule-".$suffix);
    foreach($map as $cle => $valeur) {
      $xmlWriter->addLibElement($cle, $valeur);
    }
    $xmlWriter->closeTag("intitule-".$suffix);
  }
  
  private function getSuffix($intituleType) {
    switch ($intituleType) {
      case SDE_INTITULE_THESAURUS:
        return "thesaurus";
      default:
        throw new Exception("Wrong intituleType = " + $intituleType);
    }
  }
  
}

/**
* Implémentation de http://www.scrutari.net/dokuwiki/scrutaridata:exportapi:motcleexport
*/
class SDE_MotcleExport {
  private $ficheId = "";
  private $libelleArray = array();

    public function reinit($motcleId) {
        $this->motcleId = $motcleId;
        $this->libelleArray = array();
    }

    public function setLibelle($lang, $libelleValue) {
        $this->libelleArray[$lang] = $libelleValue;
    }

    public function writeXML(SDE_XmlWriter $xmlWriter) {
        $xmlWriter->openTagWithAttribute("motcle","motcle-id", $this->motcleId);
        foreach ($this->libelleArray as $cle => $valeur) {
            $xmlWriter->addLibElement($cle, $valeur);
        }
        $xmlWriter->closeTag("motcle");
    }
    
}

/**
* Exception renvoyée en cas d'appel à une méthode de l'API à un mauvais moment
*/
class SDE_ExportStateException extends Exception {
 
}

/**
* Classe utilitaire d'écriture de XML utilisée par les différentes classes
* $file en argument doit être un pointeur vers un fichier, s'il n'est pas défini
* le XML est écrit vers la sortie standard (commande echo)
*/
class SDE_XmlWriter {
  private $file;
  private $indentLength;

  public function __construct($file, $pretty_xml = false, $with_declaration = false) {
    $this->file = $file;
    if ($pretty_xml) {
      $this->indentLength = 0;
    } else {
      $this->indentLength = -999999;
    }
    if ($with_declaration) {
      $this->appendDeclaration();
    }
  }

  public function appendDeclaration() {
    $this->write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
    if ($this->indentLength < 0) {
      $this->write("\n");
    }
  }

  public function openTag($tagName, $increaseIndent = true) {
    $this->appendIndent();
    $this->write('<');
    $this->write($tagName);
    $this->write('>');
    if ($increaseIndent) {
      $this->increaseIndentValue();
    }
  }
        
  public function openTagWithAttribute($tagName, $attributeName, $attributeValue, $increaseIndent = true) {
    $this->appendIndent();
    $this->write('<');
    $this->write($tagName);
    $this->addAttribute($attributeName, $attributeValue);
    $this->write('>');
    if ($increaseIndent) {
      $this->increaseIndentValue();
    }
  }
  
  public function openTagWithAttributes($tagName, $attributesMap, $increaseIndent = true) {
    $this->appendIndent();
    $this->write('<');
    $this->write($tagName);
    foreach($attributesMap as $attributeName => $attributeValue) {
      $this->addAttribute($attributeName, $attributeValue);
    }
    $this->write('>');
    if ($increaseIndent) {
      $this->increaseIndentValue();
    }
  }
  
  public function addAttribute( $attributeName, $attributeValue) {
    if (strlen($attributeValue) == 0) return;
    $this->write(' ');
    $this->write($attributeName);
    $this->write('=');
    $this->write('"');
    $this->escape($attributeValue);
    $this->write('"');
  }
  
  public function escape($value) {
    $this->write(htmlspecialchars($value, ENT_QUOTES));
  }

  public function closeTag($tagName, $decreaseIndent = true) {
    if ($decreaseIndent) {
      $this->decreaseIndentValue();
      $this->appendIndent();
    }
    $this->write('</');
    $this->write($tagName);
    $this->write('>');
  }

  public function addSimpleElement($tagName, $value) {
    if (strlen($value) == 0) return;
    $this->openTag($tagName, false);
    $this->escape($value);
    $this->closeTag($tagName, false);
  }
  
  public function addEmptyElement($tagName, $attributesMap) {
    $this->appendIndent();
    $this->write('<');
    $this->write($tagName);
    if (!is_Null($attributesMap)) {
      foreach($attributesMap as $attributeName => $attributeValue) {
        $this->addAttribute($attributeName, $attributeValue);
      }
    }
    $this->write('/>');
  }
     
  public function addLibElement($lang,$value) {
    $this->appendIndent();
    $this->write('<lib xml:lang="');
    $this->write($lang);
    $this->write('">');
    $this->escape($value);
    $this->write('</lib>');
  }
  
  private function appendIndent() {
    if ($this->indentLength > - 1) {
      $this->write("\n");
      for($i = 0; $i < $this->indentLength; $i++) {
        $this->write("\t");
      }
    }
  }
               
  private function increaseIndentValue() {
    $this->indentLength = $this->indentLength + 1;
  }

  private function decreaseIndentValue() {
    $this->indentLength = $this->indentLength - 1;
  }
 
  private function write($text) {
    if ($this->file) {
        fwrite($this->file,$text);
    }
    else {
        print($text);
    }
  }
  
}