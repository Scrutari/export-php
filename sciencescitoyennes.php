<?php
/*************************************************************
* Script d'extraction des données au format ScrutariData pour le site http://sciencescitoyennes.org/
*
* www.comedie.org est un site sous Wordpress, ce script interroge directement la base de données du site
* pour extraire les posts de la partie Ressources du site.
*
* Ce script doit être appelé par un autre script qui doit avoir défini les constantes suivantes:
* DB_HOST : serveur de la base MySql
* DB_NAME : nom de la base MySql
* DB_USER : utilisateur de la base MySql
* DB_PASSWORD : mot de passe d'accès à la base
* DB_CHARSET : jeu de caractères de la base  MySql
*
* Ces constantes sont les mêmes que celle du fichier wp_config.php.
*
* Le script appelant doit également avoir défini la constance : SCRUTARIDATA_PATH qui est le chemin du fichier
* dans lequel sera enregistré l'extraction. Si sa valeur est une chaine vide, l'extraction sera envoyée vers la sortie.
* Exemple :
* define('SCRUTARIDATA_PATH', 'sciencescitoyennes.scrutari-data.xml');
* define('SCRUTARIDATA_PATH', '');
*************************************************************/

//Chargement de la bibliothèque ScrutariDataExport, supposée être dans le même répertoire que ce script
require_once("scrutaridataexport.php");

define("TABLE_PREFIX","1agm25");

//Identifiants des posts à ignorer (cas particulier non traités)
$posts_to_ignore = array(52, 1329, 1332, 1338, 1339, 1340, 1341, 1342,1345,1347,1349,1599,2025,2054,
20827,25605,25595,25599);

//Identifiants de posts en anglais
$english_posts = array(1588, 1589, 1664, 1773, 1778, 1779, 1796, 1798, 1800, 2126);


//Identifiants des termes à ignorer
$terms_to_ignore = array(4, 26, 30, 35, 36, 57, 63, 75, 77, 78, 82, 83, 85, 87, 89, 90, 199, 200, 207,216, 220, 221, 222, 223, 224, 225, 226, 232, 233, 234, 236, 237, 238, 239, 243, 244, 245, 251, 252, 254, 259, 260, 262, 263, 265, 267, 268, 335, 317, 405, 416, 418, 483, 486, 542, 544, 545, 558, 580, 589, 907);

//Identifiants des termes en anglais
$english_terms = array(46, 572, 573, 574, 575, 576, 577, 579, 900);


/**
* Recensement des posts Wordpress retenus pour l'extraction, avec indication du nom du corpus correspondant
*/
class PostCensus {

    private $map;

    function __construct() {
        $this->map = array();
    }
    
    function put($id, $corpus) {
        $this->map["id_".$id] = $corpus;
    }
    
    function get($id) {
        return $this->map["id_".$id];
    }

}

/**
* Complète le tableau $posts_to_ignore avec les posts indexés par le mot-clé archives
*/
function populatePostsToIgnore($pdo) {
    global $posts_to_ignore;
    $postStatement = $pdo->query("SELECT object_id FROM ".TABLE_PREFIX."_term_relationships WHERE term_taxonomy_id=4");
    while ($postId = $postStatement->fetchColumn(0)) {
        $posts_to_ignore[] = $postId;
    }
}

/**
* Ajoute les posts appartenant à la catégorie indiquée par $termTaxonomyId,
* ces posts étant destiné à être inclus dans le corpus de nom $corpusName
*/
function addPosts($pdo, $scrutariDataExport, $termTaxonomyId, $postCensus, $corpusName, $lang) {
    global $posts_to_ignore;
    global $english_posts;
    $statement = $pdo->query("SELECT object_id FROM `".TABLE_PREFIX."_term_relationships` WHERE `term_taxonomy_id` = ".$termTaxonomyId);
    $array = array();
    while ($row = $statement->fetch(PDO::FETCH_NUM)) {
        $id = $row[0];
        if (!in_array($id, $posts_to_ignore)) {
            $array[] = $id;
        }
    }
    foreach($array as $postId) {
        $postStatement =  $pdo->query("SELECT * FROM `".TABLE_PREFIX."_posts` WHERE `ID` = ".$postId." AND post_status='publish'");
        $postRow = $postStatement->fetch(PDO::FETCH_ASSOC);
        if ($postRow) {
            $postCensus->put($postId, $corpusName);
            $titre = $postRow['post_title'];
            $soustitre = trim(strip_tags($postRow['post_content']));
            $idx = strpos($soustitre, "\n");
            if (strpos($soustitre, "[caption") === 0) { //cas des textes commençant par [caption"
                if ($idx > 0) {
                  $soustitre = trim(substr($soustitre, $idx+1));
                  $idx = strpos($soustitre, "\n");
                } else {
                  $soustitre = "";
                }
            }
            if ($idx > 0) {
                $soustitre = trim(substr($soustitre, 0, $idx));
            }
            if (strlen($soustitre) > 150) {
                $idx2 = strpos($soustitre, ".", 150);
                if ($idx2 > 0) {
                    $soustitre = substr($soustitre, 0, $idx2);
                }
            }
            $date = $postRow['post_date'];
            if (strlen($date) > 10) {
                $date = substr($date, 0, 10);
            }
            $posts_to_ignore[] = $postId;
            $ficheExport = $scrutariDataExport->newFiche($postId);
            $ficheExport->setTitre($titre);
            $ficheExport->setSoustitre($soustitre);
            $ficheExport->setHref("http://sciencescitoyennes.org/?p=".$postId);
            if (in_array($postId, $english_posts)) {
                $ficheExport->setLang('en');
            } else {
                $ficheExport->setLang($lang);
            }
            if (strlen($date) > 0) {
                $ficheExport->setDate($date);
            }
            getAuteurs($postId, $pdo, $ficheExport);
        }
    }
}


/**
* Récupère le nom des auteurs d'un post
*/
function getAuteurs($postId, $pdo, $ficheExport) {
    $query = "SELECT taxonomy.term_taxonomy_id,term.name, user.display_name FROM ".TABLE_PREFIX."_term_relationships AS relation, ".TABLE_PREFIX."_term_taxonomy AS taxonomy, ".TABLE_PREFIX."_terms AS term, ".TABLE_PREFIX."_users AS user WHERE relation.term_taxonomy_id = taxonomy.term_taxonomy_id AND taxonomy.term_id = term.term_id AND user.user_login = term.name AND taxonomy.taxonomy = 'author' AND relation.object_id = ".$postId." ORDER BY relation.term_order";
    $statement = $pdo->query($query);
    while ($row = $statement->fetch(PDO::FETCH_NUM)) {
        $id = $row[0];
        if (($id != 505) && ($id != 456) && ($id != 444)) {
            $ficheExport->addAttributeValue("sct", "authors", $row[2]);
        }
    }
}

/**
* Ajoute les termes de la taxonomie $taxonomy sous la forme de mots-clés du thésaurus de nom $thesaurusName,
* la fonction récupère également les posts liés au terme et crée une indexation si le post a bien été recensé
* préalablement dans $postCensus
*/
function addTerms($pdo, $scrutariDataExport, $taxonomy, $postCensus, $thesaurusName) {
    global $terms_to_ignore;
    global $english_terms;
    $query = "SELECT ".TABLE_PREFIX."_term_taxonomy.term_taxonomy_id, ".TABLE_PREFIX."_terms.name  FROM ".TABLE_PREFIX."_terms,".TABLE_PREFIX."_term_taxonomy WHERE ".TABLE_PREFIX."_term_taxonomy.term_id = ".TABLE_PREFIX."_terms.term_id AND ".TABLE_PREFIX."_term_taxonomy.taxonomy = '".$taxonomy."'";
    $statement = $pdo->query($query);
    $array = array();
    while ($row = $statement->fetch(PDO::FETCH_NUM)) {
        $id = $row[0];
        $name = $row[1];
        if ((strlen($name) > 0) && (!in_array($id, $terms_to_ignore))) {
            $array[] = $id;
            $motcleExport = $scrutariDataExport->newMotcle($id);
            $lang = "fr";
            if (in_array($id, $english_terms)) {
                $lang = "en";
            }
            $motcleExport->setLibelle($lang, $name);
        }
    }
    foreach($array as $termId) {
        $postStatement = $pdo->query("SELECT object_id FROM ".TABLE_PREFIX."_term_relationships WHERE term_taxonomy_id=".$termId);
        while ($postId = $postStatement->fetchColumn(0)) {
            $corpusName = $postCensus->get($postId);
            if (strlen($corpusName) > 0) {
                $scrutariDataExport->addIndexation($corpusName, $postId, $thesaurusName, $termId,1);
            }
        }
    }
}



/*************************************************************
* Initialisation
**************************************************************/

//Test si l'extraction est écrite dans un fichier ou directement vers la sortie
$file = false;
if (strlen(SCRUTARIDATA_PATH) > 0) {
    $file = fopen(SCRUTARIDATA_PATH, "w");
}

//Accès à la base de données
$pdo = new PDO('mysql:host='.DB_HOST.';dbname='.DB_NAME.';charset='.DB_CHARSET, DB_USER, DB_PASSWORD);

// Instance de PostCensus recensant les posts inclus dans l'extraction
$postCensus = new  PostCensus();

// Instance de SDE_XmlWriter recensant les posts inclus dans l'extraction
$xmlWriter = new SDE_XmlWriter($file, true, true);
$scrutariDataExport = new SDE_ScrutariDataExport($xmlWriter);

//complète le tableau $posts_to_ignore
populatePostsToIgnore($pdo);

/*************************************************************
* Exportation au format ScrutariData
**************************************************************/

//Démarrage de l'export avec la définition des méta-données
$baseMetadataExport = $scrutariDataExport->startExport();
$baseMetadataExport->setAuthority("sciencescitoyennes");
$baseMetadataExport->setBaseName("site");
$baseMetadataExport->setBaseIcon("http://scrutari.coredem.info/sciencescitoyennes.png");
$baseMetadataExport->setIntitule(SDE_INTITULE_SHORT, "fr", "Sciences Citoyennes");
$baseMetadataExport->setIntitule(SDE_INTITULE_LONG, "fr", "Sciences Citoyennes - Réappropriation citoyenne et démocratique de la science");
$baseMetadataExport->addLangUI("fr");

//Création du corpus synthese correspondant aux notes de synthese
$corpusMetadataExport = $scrutariDataExport->newCorpus("synthese");
$corpusMetadataExport->setIntitule(SDE_INTITULE_CORPUS, "fr","Note de synthèse");
$corpusMetadataExport->setIntitule(SDE_INTITULE_FICHE, "fr", "Note de synthèse n°");

//Ajout des posts des synthese (12 est l'identifiant du terme correspondant à la catégorie des expériences)
addPosts($pdo, $scrutariDataExport, 399, $postCensus, "synthese", 'fr');

//Création du corpus synthese correspondant aux notes de synthese
$corpusMetadataExport = $scrutariDataExport->newCorpus("lire");
$corpusMetadataExport->setIntitule(SDE_INTITULE_CORPUS, "fr","À lire");
$corpusMetadataExport->setIntitule(SDE_INTITULE_FICHE, "fr", "À lire n°");

//Ajout des posts des synthese (12 est l'identifiant du terme correspondant à la catégorie des expériences)
addPosts($pdo, $scrutariDataExport, 394, $postCensus, "lire", 'fr');
addPosts($pdo, $scrutariDataExport, 565, $postCensus, "lire", 'en');

//Création du thésaurus tag
$thesaurusMetadataExport = $scrutariDataExport->newThesaurus("tag");
$thesaurusMetadataExport->setIntitule(SDE_INTITULE_THESAURUS,"fr","Mots-clés");

//Ajout des termes, les mots-clés sont distingués des autres termes par la taxonomie post_tags
addTerms($pdo, $scrutariDataExport, "post_tag", $postCensus, "tag");

//Fin de l'export
$scrutariDataExport->endExport();





