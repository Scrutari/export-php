<?php
/*****************************************************
* Exemple d'appel de sciencescitoyennes.php,
* Cet appel affiche le XML directement dans la sortie
******************************************************/

//Indication que les données transmises sont du XML
header("content-type: text/xml;charset=UTF-8");

// Nom de la base de données de WordPress.
define('DB_NAME', '*');

// Utilisateur de la base de données MySQL.
define('DB_USER', '*');

// Mot de passe de la base de données MySQL.
define('DB_PASSWORD', '*');

// Adresse de l'hébergement MySQL.
define('DB_HOST', 'localhost');

// Jeu de caractères à utiliser par la base de données
define('DB_CHARSET', 'utf8');

// Chaine vide pour SCRUTARIDATA_PATH, les données XML sont affichées directement en sortie
define('SCRUTARIDATA_PATH', '');

// Appel de sciencescitoyennes.php
require("sciencescitoyennes.php");