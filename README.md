# Scripts PHP de « scrutarisation »

Ce dépôt contient des scripts écrits en PHP pour interroger des données directement dans le format SQL et produire une sortie au format ScrutariData.

Le fichier scrutaridataexport.php est l'implémentation en PHP de l'API ScrutariDataExport.

Les scripts sont sous licence MIT.

## Pour en savoir plus :
- Scrutari : http://www.scrutari.net/
- API ScrutariDataExport : http://www.scrutari.net/dokuwiki/scrutaridata:exportapi
- sur les scripts PHP : http://www.scrutari.net/dokuwiki/scrutaridata:exportapi:python
